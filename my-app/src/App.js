import React, { useState } from "react";
import { Routes, Route } from "react-router-dom";
import { Header }  from "./components/Header/Header";
import { useLocalStorageList } from "./hooks/useLocalStorage";
import { useFetch } from "./hooks/useFetch";
import  {Home} from "./pages/Home";
import  { Basket } from "./pages/Basket";
import {Favorite } from "./pages/Favorite";
import "./components/proptypes";
import { modalWindowDeclarations } from "./components/Modal/modalWindow";


function App() {

  const [favorId, setFavorId] = useLocalStorageList("favorId");
  const [cartId, setCartId] = useLocalStorageList("cartId");
  const [products, setProducts] = useLocalStorageList("products");
  const [open, setOpen] = useState(false);
  const [currentproduct, setCurrentProduct] = useState([]);
  const [content, setContent] = useState({
    header: "",
    text: "",
    actions: [],
    closeButton: true,
});
  
  useFetch(setProducts, "./flowers.json" );

  const closeModal = (e) => {
    console.log(e.target);
    e.target.closest(".active") ? setOpen(false) : setOpen(true);
}

const showModal = (e) => {
       let modalId = e.target.getAttribute("id");
      
       setCurrentProduct(modalId);

       let modalData = e.target.getAttribute("data-modal-id");
       let currentItem = modalWindowDeclarations.find(item => item.id === modalData);
       setOpen(true);
       setContent({...currentItem});
       }
 

  return (
    <>
     <Header count={favorId.length} cartCount={cartId.length} />
    <Routes>
      <Route path="/" element={<Home open={open} setOpen = {setOpen} show = {showModal} closeModal={closeModal} content={content} currentproduct={currentproduct} setCurrentProduct = {setCurrentProduct} favorId={favorId} setFavorId={setFavorId} cartId={cartId} setCartId={setCartId} products = {products}/>}/>
      <Route path="/basket" element={<Basket open={open} setOpen = {setOpen} closeModal ={closeModal} content={content} setContent = {setContent} currentproduct={currentproduct} setCurrentProduct = {setCurrentProduct} cartId={cartId} setCartId={setCartId} products = {products} setFavorId={setFavorId}/>}/>
      <Route path="/favorite" element={<Favorite favorId={favorId} setFavorId={setFavorId} products = {products}/>}/>
    </Routes>
      
    </>
  );
}

export default App;


