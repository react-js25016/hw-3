import React from "react";
import ProductCard from "../Productсart/Productcard";
import "./cardbox.scss";
import Button from "../Button/Button";

function ProductList(props) {
        return (
            <>
                <div className="card-box" onClick={props.click}>
                {props.products.map((item) => {
                        return (
                            <div key={item.code} className="card" id={item.code}>
                                <ProductCard show ={props.show} id={item.code} src={item.url} name={item.name} price={item.price} code={item.code} color={item.color} {...item} />
                                <div className="button-wrapper">
                                <Button code="3" onClick = {props.onClick} color="pink" text="Add to cart" id={item.code}/>
                                </div>
                            </div>
                        )
                    })}
                </div>
            </>
        );
}

export default ProductList;