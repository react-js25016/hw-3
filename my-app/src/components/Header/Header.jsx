import Cart from "../Cart/Cart";
import "./header.scss";
import Logo2 from "../../img/star2.svg";
import { Link } from "react-router-dom";

export function Header(props){
    return (
                    <>
                    <div className="header">
                     <Link className="header-link" to="/">Home</Link>
                     <Link className="header-link" to="/basket">Cart</Link>
                     <Link className="header-link" to="/favorite">Favorite</Link>
                    
                        <div className="header-container">
                        <p className="current-number">{props.count}</p>
                        <img alt="favorite" src= {Logo2} num={props.count}></img>
                        <Cart num={props.cartCount} />
                        </div>
                        </div>
                    </>
                )
}