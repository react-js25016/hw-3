import React, { useState, useEffect } from "react";
import "./productcard.scss";
import Logo1 from "../../img/star1.svg";
import Logo2 from "../../img/star2.svg";


function ProductCard(props){
    const [fill, setFill] = useState(false);
    const addFavorits = () => {
                 fill === true ? setFill(false) : setFill(true);
               
             }

    useEffect(() => {
        let dataLS = localStorage.getItem("favorId");
        let arr = JSON.parse(dataLS);
            let currentItem = arr.find(item => item === props.id);
        currentItem ? setFill(true) : setFill(false);
    }, [fill, props.id])

    return(
        <>
                 <div id={props.id} className="product-card">
                     <img alt="photo_flower" className="product-card-img" src={props.src}  width={350} height={230}></img>
                     <div className="product-card-content">
                     <h4 className="product-card-name">Name: {props.name}</h4>
                     <p className="product-card-price">Price: {props.price} ₴</p>
                     <p className="product-card-code">Code: {props.code}</p>
                     <p className="product-card-color">Color: {props.color}</p>
                     <img  className = {fill === true ? "removed-from-favorite" : "add-to-favorite"} id={props.id} alt="logo" src={fill === true ? Logo1 : Logo2 } onClick={addFavorits}></img>
                     </div>
                 </div>
             </>
    )
}

export default ProductCard;