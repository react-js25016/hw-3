export const modalWindowDeclarations = [
    {
        id: '1',
        header: "Do you want to delete this file?",
        closeButton: true,
        text: 'Once you delete this file, it won`t be possible to undo this action. Are you sure you want to delete it?',
        actions: ["Ok", "Cancel"],
      },

      {
        id: '2',
        header: "Removing an Item from the cart",
        closeButton: false,
        text: 'Are you sure you want to remove an item from your shopping cart?',
        actions: ["Ok", "Cancel"],
      },

      {
        id: '3',
        header: "Add product to cart?",
        closeButton: true,
        text: 'Confirmation of adding a product to the cart',
        actions: ["Save", "Close"],
      },
]