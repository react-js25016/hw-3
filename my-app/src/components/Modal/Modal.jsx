import "./modal.scss";

function Modal(props){
    return (
        <>
                 <div className={props.className} onClick={props.close}>
                     <div className="modal" onClick={(e) => e.stopPropagation()}>
                         <div className="modal-header">
                             <h4 className="title">{props.header}</h4>
                             <span onClick={props.close} className="header-close">{props.closeButton ? "╳" : ""}</span>
                         </div>
                         <div className="modal-content">
                             <div className="modal-body">
                                 <p className="text">{props.text}</p>
                             </div>
                             <div className="modal-footer">
                                 <button className="btn" onClick={props.save}>{props.actions[0]}</button>
                                 <button className="btn" onClick={props.close}>{props.actions[1]}</button>
                             </div></div>
                     </div>
                </div>
             </>
    )
}

export default Modal;

