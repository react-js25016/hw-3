import ProductCard from "../components/Productсart/Productcard";
import { useProductList } from "../hooks/useProductList";


export function Favorite(props){

    const [ arrfavor, setarrfavor ] = useProductList(props.favorId, props.products);

    
    const removeFavor = (e, id) => {
        if(e.target.tagName !== "IMG") return false;
        id = e.target.id;
        props.setFavorId((prev) => ([...prev, id].filter(el => el !== e.target.id)));
        
    }

   
    return(
        <>
             <div className="card-box">
                {!arrfavor.length ? "There are no products in the favorites": arrfavor.map((item) => {
                    return (
                        <div key={item.code} className="card" id={item.code} onClick={removeFavor}>
                                <ProductCard show={props.show} id={item.code} src={item.url} name={item.name} price={item.price} code={item.code} color={item.color} {...item} />
                        </div>
                    )
                })}
            </div>
        </>
    )
    
}