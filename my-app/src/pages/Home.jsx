import ProductList from "../components/ProductList/ProductList";
import Modal from "../components/Modal/Modal";


export function Home(props){
    
    const addFavorites = (e, id) => {
                    if(e.target.tagName !== "IMG") return false;
                    id = e.target.id;
                  e.target.className === "add-to-favorite" ?
                    props.setFavorId((prev) => ([...prev, id])) :
                    props.setFavorId((prev) => ([...prev, id].filter(el => el !== e.target.id)))
                }

                    
    const addCart = (e) => {
            let isInArray = false;
            props.cartId.forEach(item => {
                if(item === props.currentproduct){
                    isInArray = true;
                }
            })
            if(!isInArray){
                props.setCartId((prev) => ([...prev, props.currentproduct]));
            }
            
            
            e.target.closest(".active") ? props.setOpen( false ) : props.setOpen( true );
                        }

            
    return (
                    <>
                        <ProductList click={addFavorites} onClick={ props.show } products={props.products} />
                        <Modal
                            className={props.open ? "modal-wrapper active" : "modal-wrapper"}
                            save={addCart}
                            close={props.closeModal}
                            header={props.content.header}
                            text={props.content.text}
                            closeButton={props.content.closeButton}
                            actions={props.content.actions}
                        />
                    </>
                )
}