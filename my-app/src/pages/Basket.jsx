import ProductCard from "../components/Productсart/Productcard";
import Modal from "../components/Modal/Modal";
import { modalWindowDeclarations } from "../components/Modal/modalWindow";
import "../components/Button/button.scss";
import { useProductList } from "../hooks/useProductList";

export function Basket(props) {

const [ arrcard, setarrcard ] = useProductList(props.cartId, props.products);

const removeCart = (e) => {
    
    props.setCartId((prev) => ([...prev, props.currentproduct].filter(el => el !== props.currentproduct)));
    
    e.target.closest(".active") ? props.setOpen( false ) : props.setOpen( true );
                }

    const showModal = (e) => {
        let modalId = e.target.getAttribute("data-num");
        props.setCurrentProduct(modalId);

        let modalData = e.target.getAttribute("data-modal-id");
        let currentItem = modalWindowDeclarations.find(item => item.id === modalData);
        props.setOpen(true);
        props.setContent({...currentItem});
        }

        const addFavorites = (e, id) => {
            if(e.target.tagName !== "IMG") return false;
            id = e.target.id;
          e.target.className === "add-to-favorite" ?
            props.setFavorId((prev) => ([...prev, id])) :
            props.setFavorId((prev) => ([...prev, id].filter(el => el !== e.target.id)))
        }

    return (
        <>
            <div className="card-box" onClick={addFavorites}>
            {!arrcard.length ? "There are no products in the cart" : arrcard.map((item) => {
                    return (
                        <>
                        <div key={Date.now()/2} className="card">
                                <ProductCard show={props.show} id={item.code} src={item.url} name={item.name} price={item.price} code={item.code} color={item.color} {...item} />
                                
                            <Modal
                            className={props.open ? "modal-wrapper active" : "modal-wrapper"}
                            save={removeCart}
                            close={props.closeModal}
                            header={props.content.header}
                            text={props.content.text}
                            closeButton={props.content.closeButton}
                            actions={props.content.actions}
                        />
                        </div>
                        <button key={Date.now()} className="delete-btn" data-num={item.code} data-modal-id={2} onClick={showModal}>X</button>
                        </>
                    )
                })}
            </div>
            
        </>
    );
}