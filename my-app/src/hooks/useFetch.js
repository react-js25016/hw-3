import { useEffect } from "react";

export const useFetch = (setState, url) => {useEffect( () => {
    try {
        fetch(url)
            .then(response => response.json())
            .then(data => setState([...data]))
    } catch (e) {
        console.log(e.message);
    }}, [])} ;